"use strict";
let datafire = require('datafire');

let google_classroom = require('@datafire/google_classroom').actions;
module.exports = new datafire.Action({
  handler: async (input, context) => {
    let courseWork = await Promise.all([].map(item => google_classroom.courses.courseWork.get({
      courseId: "",
      id: "",
    }, context)));
    return courseWork;
  },
});
